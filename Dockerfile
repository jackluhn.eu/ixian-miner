# Define base image
FROM mono:latest AS build-env
WORKDIR /source
RUN apt update  && \
    apt install -y ca-certificates-mono make gcc 


COPY ["./Ixian-Miner/", "/source/Ixian-Miner/"]
COPY ["./Ixian-Core/", "/source/Ixian-Core/"]

COPY ["./phc-winner-argon2/", "/source/phc-winner-argon2/"]

RUN  cd /source/Ixian-Miner/ && \
     nuget restore IxianMiner.sln && \
     msbuild IxianMiner.sln /p:Configuration=Release && \
     cd /source/phc-winner-argon2/ && \
     make && \
     ls | grep libargon2.so.1 && \
     cp libargon2.so.1 /source/Ixian-Miner/IxianMiner/bin/Release/libargon2.so



FROM mono:latest
WORKDIR /opt/app
COPY --from=build-env /source/Ixian-Miner/IxianMiner/bin/Release/ /opt/app/

COPY ./entrypoint.sh /opt/entrypoint.sh
ENTRYPOINT /opt/entrypoint.sh
